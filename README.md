App Engine Java with Spring MVC and Angular2 frontend

* Uses AppEngine SDK 1.9.40
* Spring 4.3.6
* NodeJs 6.10.x

Requires [Apache Maven](http://maven.apache.org) 3.1 or greater, Node 6.10+ and JDK 7+ in order to run.

*Note: On Windows the maven-exec-plugin can call the npm.sh and ng.sh files in PATH, to solve that rename/remove the files
 or change pom.xml command from "npm" to "npm.cmd" and from "ng" to "ng.cmd" LOCALLY ONLY

Make sure you are running as administrator/root permissions

To build, run

    mvn clean install

Building will run the tests, but to explicitly run tests you can use the test target

    mvn test

To start the app, use the [App Engine Maven Plugin](http://code.google.com/p/appengine-maven-plugin/) that is already included in this demo.  Just run the command.

    mvn appengine:devserver

To Update the app in appengine project, just run the command and log into you google account authorized for tha app

    mvn appengine:update

For further information, consult the [Java App Engine](https://developers.google.com/appengine/docs/java/overview) or [Angular2](https://angular.io/docs/ts/latest/) documentation.